# eselect-ld




## Usage

To set new ld symlink target use
```
eselect ld set <TARGET>
```
where `TARGET` is either a number from
```
eselect ld list
```
Alternatively use the name of a linker directly. Name may be
prefixed by `ld.`.
```
eselect ld set mold
```
```
eselect ld set ld.mold
```
To print current symlink target use
```
eselect ld show
```

## Installation

Copy into eselect's module directory.
```
cp ./ld.eselect /usr/share/eselect/modules/.
```
